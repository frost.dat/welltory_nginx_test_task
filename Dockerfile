FROM ubuntu:latest

ENV TZ=Europe/Moscow

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone \
    && apt-get update \
    && apt-get install build-essential libpcre++-dev libssl-dev wget zlibc zlib1g zlib1g-dev -y \
    && apt-get clean

RUN cd /tmp/ \
    && wget http://nginx.org/download/nginx-1.17.10.tar.gz \
    && tar -zxvf nginx-1.17.10.tar.gz \
    && rm nginx-1.17.10.tar.gz \
    && cd nginx-1.17.10 \
    && ./configure \
        --prefix=/usr/local/nginx \
        --user=nginx-user \
        --group=nginx-user \
        --with-http_stub_status_module \
    && make \
    && make install

RUN useradd -s /usr/sbin/nologin nginx-user \
    && ln -s /usr/local/nginx/sbin/nginx /bin/nginx \
    && nginx -V

COPY nginx.conf /usr/local/nginx/conf/nginx.conf

CMD ["nginx"]

EXPOSE 80
