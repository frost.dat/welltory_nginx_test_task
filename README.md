# NGINX v1.17.10

![](https://img.shields.io/gitlab/pipeline/frost.dat/welltory_nginx_test_task)

## История изменения проекта:
v 0.01

<code>NGINX v1.17.10</code> собранный из [исходного кода](https://nginx.org/ru/download.html) с 
установленным модулем <code>http_stub_status_module</code>
Используемые каталоги используются по умолчанию. 

v 0.02

